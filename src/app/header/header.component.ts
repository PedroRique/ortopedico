import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  public isOpen = true;

  constructor() { }

  ngOnInit() {
  }

  toggleMenu(){
    this.isOpen = !this.isOpen;
  }

  onNotify(e){
    this.isOpen = e;
  }

}
