import { Component, OnInit, Inject} from '@angular/core';

@Component({
  selector: 'app-alugue',
  templateUrl: './alugue.component.html',
  styleUrls: ['./alugue.component.sass']
})
export class AlugueComponent implements OnInit {

  constructor(@Inject("windowObject") private window: {LC_API:any}){

  }

  ngOnInit() {
  }

  openChat(){
    this.window.LC_API.open_chat_window();
  }
}
