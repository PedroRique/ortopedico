import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.sass']
})
export class ProdutosComponent implements OnInit {

  public produtos = [];
  public alugueis = [];

  constructor() {
    this.produtos = [
      {
        preco: '449.90',
        nome: 'Cadeira De Rodas Econômica',
        desc: 'Dobrável, largura total 45cm.',
        img: 'cadeira-de-rodas',
        id: 1
      },
      {
        preco: '299.90',
        nome: 'Cadeira de Banho',
        desc: 'Largura total 45cm, suporta até 85kg, cadeira de aço.',
        img: 'cadeira-de-banho',
        id: 2
      },
      {
        preco: '99.90',
        nome: 'Par de Muletas',
        desc: 'Muletas simples, suportam até 90kg',
        img: 'pardemuleta',
        id: 3
      },
      {
        preco: '49.90',
        nome: 'Tipoia Imobilizadora',
        desc: 'Bilateral, temos todos os tamanhos disponíveis.',
        img: 'tipoia',
        id: 4
      },
      {
        preco: '129.90',
        nome: 'Imobilizador De Joelho',
        desc: 'Bilateral com tamanho ajustável.',
        img: 'joelho',
        id: 5
      },
      {
        preco: '199.90',
        nome: 'Andador',
        desc: 'Dobrável e articulado. Suporta até 85kg.',
        img: 'andador',
        id: 6
      },
      {
        preco: '129.90',
        nome: 'Bota Imobilizadora Robocop',
        desc: 'Bota robofoot, todos os tamanhos, bilateral.',
        img: 'bota-robocop',
        id: 7
      },
      {
        preco: '449.90',
        nome: 'Colchão pneumático',
        desc: 'Pressão alternada. Solução ideal para pacientes acamados.',
        img: 'colchao-pneumatico',
        id: 8
      }
    ]
  
    this.alugueis = [
      {
        preco: '189.90',
        nome: 'Cadeira De Rodas Econômica',
        desc: 'Aluguel de 30 dias. Dobrável, largura total 45cm. Alugue também 15 dias por 129,90.',
        img: 'cadeira-de-rodas',
        id: 1
      },
      {
        preco: '89.90',
        nome: 'Cadeira de Banho',
        desc: 'Aluguel de 30 dias. Largura total 45cm, suporta até 85kg, cadeira de aço.',
        img: 'cadeira-de-banho',
        id: 2
      },
      {
        preco: '49.90',
        nome: 'Par de Muletas',
        desc: 'Aluguel de 30 dias. Muletas simples, suportam até 90kg. Alugue também 15 dias por 29,90.',
        img: 'pardemuleta',
        id: 3
      },
      {
        preco: '69.90',
        nome: 'Andador',
        desc: 'Aluguel de 30 dias. Dobrável e articulado, suporta até 85kg.',
        img: 'andador',
        id: 4
      }
    ]
  }

  ngOnInit() {
  }

  encodeURIComponent(r) {
    return encodeURIComponent(r);
  }

}
