import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.sass']
})
export class BannerComponent implements OnInit {

  
  slides = [
    {
      img: "banner-cadeira-economica",
      big: 'Econômica',
      word: 'Cadeira',
      wordb: 'Aluguel',
      sub: 'Melhor custo benefício',
      preco: '129,90',
      aluguel: true,
      diasAluguel: 15
    },
    {
      img: "banner-cadeira-banho",
      big: 'Higiênica',
      word: 'Cadeira',
      wordb: 'Aluguel',
      sub: 'Conforto em todos os momentos',
      preco: '89,90',
      aluguel: true,
      diasAluguel: 30
    },
    {
      img: "banner-andador",
      big: 'Articulado',
      word: 'Andador',
      wordb: 'Aluguel',
      sub: 'Mais segurança para sua vida',
      preco: '69,90',
      aluguel: true,
      diasAluguel: 30
    },
    {
      img: "banner-muletas",
      big: 'Muletas',
      word: 'Par de',
      wordb: 'Aluguel',
      sub: 'Mais segurança para sua vida',
      preco: '29,90',
      aluguel: true,
      diasAluguel: 15
    }
  ];
  slideConfig = {slidesToShow: 1, slidesToScroll: 1, dots: true, arrows: true, autoplay: true, autoplaySpeed: 2000};

  constructor() { }

  ngOnInit() {
  }

  setBanner(slide){
    return {'background-image': `url(assets/img/${slide.img}.jpg)`};
  }

}
