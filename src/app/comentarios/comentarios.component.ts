import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.sass']
})
export class ComentariosComponent implements OnInit {

  public comments = [
    {
      text: 'O atendimento é super profissional, o produto chegou em perfeito estado. Não encontrei melhor preço no mercado.',
      img: 'avatar1'
    },
    {
      text: 'Loja excelente! Atendimento maravilhoso. Produtos de primeira qualidade e primeira linha! Entrega muito rápida. O produto chegou em minha casa em 40 minutos.',
      img: 'avatar2'
    },
    {
      text: 'Excelente atendimento com boas orientações. Percebi o interesse em me oferecer a melhor opção de equipamento e não a mais cara, como se faz na maioria dos estabelecimentos. Indico uma visita com certeza!',
      img: 'avatar3'
    }
  ];

  slideConfig = {slidesToShow: 1, slidesToScroll: 1, dots: true, arrows: true, autoplay: true, autoplaySpeed: 3000};

  constructor() { }

  ngOnInit() {
  }

}
