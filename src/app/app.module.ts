import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { HttpClientModule } from '@angular/common/http';
import { NgtUniversalModule } from '@ng-toolkit/universal';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AlugueComponent } from './alugue/alugue.component';
import { TrabalhamosComponent } from './trabalhamos/trabalhamos.component';
import { ComentariosComponent } from './comentarios/comentarios.component';
import { LojasComponent } from './lojas/lojas.component';
import { EntregaComponent } from './entrega/entrega.component';
import { ProdutosComponent } from './produtos/produtos.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { MarcasComponent } from './marcas/marcas.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    AlugueComponent,
    TrabalhamosComponent,
    ComentariosComponent,
    LojasComponent,
    EntregaComponent,
    ProdutosComponent,
    HeaderComponent,
    BannerComponent,
    MarcasComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    CommonModule,
    TransferHttpCacheModule,
    HttpClientModule,
    NgtUniversalModule,
    SlickCarouselModule
  ],
  providers: [{ provide: "windowObject", useValue: window}],
  bootstrap: [AppComponent]
})
export class AppModule { }
